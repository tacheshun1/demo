package com.example.demo.controllers;

import com.example.demo.services.CatalogService;
import com.example.demo.services.ICatalogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Slf4j
@Controller
@RequestMapping("/catalog")
public class CatalogController {
    private final ICatalogService service;

    public CatalogController(ICatalogService service) {
        this.service = service;
    }

    @GetMapping
    public @ResponseBody String showCatalogPage() {
        return service.listProducts();
    }
}
